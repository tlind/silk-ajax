$.prototype.addErrors = function(errors) {
	// TODO onchange must remove the error from the field, also onsubmit
	var form = this.get(0);
	if (errors instanceof Array) {
		for (var i = 0; i < errors.length; i++) {
			var field = errors[i]['field']
			var msg = errors[i]['message']
			
			$(form[field]).addClass('error').after('<span class="error">'+msg+'</span>')
		}
	}
	else if (errors instanceof Object) {
		for (var field in errors) {
			var msg = (typeof errors[field] == 'string') ? errors[field] : errors[field].message;
			
			$(form[field]).addClass('error').after('<span class="error">'+msg+'</span>')
		}
	}
};

/* submit forms with ajax and interpret server validation errors. */

$(document).delegate('form', 'submit', function(e) {
	var form = this,
		isAsync = (form.getAttribute('async') == 'true'),
		targetName = form.getAttribute('target'),
		enctype = form.getAttribute('enctype'),
		targetNode,
		data;
	
	if (isAsync || targetName && enctype != 'multipart/form-data') {
		// default target node to the form element's parent
		if (!targetName)
			targetNode = $(form).parent(); // TODO should be the closest parent that is acting as a window.
		else
			targetNode = $('#'+targetName);
		
		if (enctype == 'json')
			// remember that on the server side this will be odd, it is an array of objects of the form { name: n, value: v }
			data = $(form).serializeArray();
		else
			data = $(form).serialize();
			
		$.ajax(this.getAttribute("action"), { 
			type: form.getAttribute("method") || "GET",
			data: data,
			success: function(responseText) {
				// TODO if the target node is a window (i.e iframe or the parent window) then change the href instead of inserting innerHTML
				// The link click plugin takes a href whereas this takes the result of a post request, which is incompatible with that plugin's functionality.
				targetNode.html(responseText);
			},
			error: function(xhr, textStatus, errThrown) {
				$(form).addErrors(JSON.parse(xhr.responseText));
			}
		});
	
		e.preventDefault();
	}
});

/* intercept link clicks to handle hash state and async fragment loads, must catch clicks within elements that are acting as frames because they have previously been targetted. */

(function() {

	$(function() {
		// TODO this is terrible parsing, having a fully featured url in the hash fragment just isn't possible
		
		
		var target = location.hash.split('=')[0], targetNode = $(target), hrefAttr = location.hash.split('=')[1];
		targetNode.navigateTo(hrefAttr);
	})
	
	var catchClicks = function (e, targetNode) {
		var target = (this.getAttribute('target')), targetNode = ((targetNode) ? targetNode : $('#'+target)), hrefAttr = this.getAttribute('href');
		// TODO are links relative to the href that is loaded in the parent element?

		if (targetNode.length > 0) {
			e.preventDefault();
			targetNode.navigateTo(hrefAttr);
		}
	};
	
	$(document).delegate('a', 'click', catchClicks);
	
	$.fn.navigateTo = function navigateTo(hrefAttr) {
		if (this.length > 0) {
			var targetNode = this[0], target = this[0].getAttribute('id'), self = this;

			$.ajax(hrefAttr, {
				cache: false,
				success: function(html) {
					if (target)
						location.hash = target + '=' + hrefAttr;
					if (!self.attr('href'))
						self.delegate('a', 'click', function(e) {
							catchClicks.call(this, e, self);
						});
					self.attr('href', hrefAttr);
					self.html(html);
					self.trigger('load');
				}
			});
		}
	}
	
})();

/* link objects / elements to data from a remote src */

// TODO ERR the data-link plugin doesn't support arrays, 
// maybe this is fine for forms, but it is not fine for other elements, we then need to use a templating plugin for those cases.
// TODO ERR we actually need two separates concepts, one for forms that get data to load,
// and another for templates that get data and evaluate, there are two separate underlying plugins,
// and templates should actually be hidden until the data and evaluation is done...or is the same true for forms?

$(document).delegate('body *', 'load', function() {
	var src = $(this).attr('data-src');
	
	if (src) {
		$(this).linkRemote(src);
	}		
});

$.fn.linkRemote = function(src, settings) {
	var t = this, objs = [], link = function(data) {
		t.link(data, settings);
	};
	
	if (typeof objs[src] == 'undefined') {
		$.getJSON(src, function(data) {
			objs[src] = data; link(data);
		})
	}
};
